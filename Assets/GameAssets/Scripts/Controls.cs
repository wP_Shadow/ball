﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Controls : MonoBehaviour   
{
    public Gradient _Gradient;
    public Material _BaseMaterial;
    public float _BoostAngle = 20;
    public float _SlowAngle = 55;
    public float _MaxAngle = 65;
    public UnityEngine.UI.Text _SpeedTextField;

    Rigidbody rig;
    int _dir = 1;
    bool _isBordered = false;
    float _BaseSpeed = 22f;
    float _MinSpeed = 15f;
    float _MaxSpeed = 25f;

    // Start is called before the first frame update
    void Start()
    {
        rig = gameObject.GetComponent<Rigidbody>();
        rig.velocity = new Vector3(0, 0, _MinSpeed);
        GradientColorKey[] arrColors = new GradientColorKey[] {
            new GradientColorKey(new Color32(154,255,95,255), _BoostAngle /_MaxAngle),
            new GradientColorKey(new Color32(200, 200, 200, 255), (_BoostAngle + 2f) / _MaxAngle),
            new GradientColorKey(new Color32(200, 200, 200, 255), (_SlowAngle - 2f) / _MaxAngle),
            new GradientColorKey(new Color32(255, 85, 85, 255), _SlowAngle / _MaxAngle) };

        _Gradient.SetKeys(arrColors, new GradientAlphaKey[] { new GradientAlphaKey(1f, 0f), new GradientAlphaKey(1f, 1f) });
    }

    // Update is called once per frame
    void Update()
    {
        if (_isBordered)
            return;

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            _dir *= -1;
        }
        
        speedRecalculate();
        changeAngle();
        _SpeedTextField.text = rig.velocity.magnitude.ToString();
    }

    private void changeAngle()
    {
        float mply = rig.velocity.magnitude / _BaseSpeed;
        mply = mply > 1 ? mply : 1;

        if (rig.velocity.x * _dir > 0)
        {
            Vector3 targetDir = Quaternion.AngleAxis(_dir * 2f * mply, Vector3.up) * rig.velocity;
            float angle = Vector3.Angle(targetDir, Vector3.forward);

            if (angle <= _MaxAngle)
                rig.velocity = targetDir;
        }
        else
        {
            Vector3 targetDir = Quaternion.AngleAxis(_dir * 3f * mply, Vector3.up) * rig.velocity;
            float angle = Vector3.Angle(targetDir, Vector3.forward);
            rig.velocity = targetDir;
        }
    }

    private void speedRecalculate()
    {
        float angleVF = Vector3.Angle(rig.velocity.normalized, Vector3.forward.normalized);
        if (angleVF < _BoostAngle)
        {
            rig.velocity += rig.velocity.normalized * 2f * Time.deltaTime;
        }
        if (angleVF > _SlowAngle)
        {
            rig.velocity *= 1f - 0.2f * Time.deltaTime;
        }
        if (rig.velocity.magnitude > _MaxSpeed)
            rig.velocity = rig.velocity.normalized * _MaxSpeed;
        if (rig.velocity.magnitude < _MinSpeed)
            rig.velocity = rig.velocity.normalized * _MinSpeed;

        _BaseMaterial.color = _Gradient.Evaluate(angleVF / _MaxAngle);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "RightCol" || other.gameObject.tag == "LeftCol")
        {
            _isBordered = true;
            _dir *= -1;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "RightCol" || other.gameObject.tag == "LeftCol")
            _isBordered = false;
    }

    private void OnTriggerStay(Collider other)
    {
        float mply = rig.velocity.magnitude / _BaseSpeed;
        mply = mply > 1 ? mply : 1;

        if (other.gameObject.tag == "RightCol" || other.gameObject.tag == "LeftCol")
        {
            Vector3 targetDir;
            if (other.gameObject.tag == "RightCol")
            {
                targetDir = Quaternion.AngleAxis(-1 * 4 * mply, Vector3.up) * rig.velocity;
            }
            else
            {
                targetDir = Quaternion.AngleAxis(1 * 4 * mply, Vector3.up) * rig.velocity;
            }

            float angle = Vector3.Angle(targetDir, Vector3.forward);

            if (angle <= _MaxAngle)
                rig.velocity = targetDir;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Obstacle")
        {
            SceneManager.LoadScene(1);
        }
    }
}
