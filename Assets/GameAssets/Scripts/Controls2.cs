﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Controls2 : MonoBehaviour
{
    public GameObject _cam;
    public GameObject _BallHolder;
    public GameObject _Ball;
    public bool isVariantA = false;
    public bool isInverseInput;

    Rigidbody rig;

    public void Restart() {
        SceneManager.LoadScene(0);
    }

    private void OnDestroy()
    {
        //Restart();
    }


    // Start is called before the first frame update
    void Start()
    {
        rig = gameObject.GetComponent<Rigidbody>();
        rig.velocity = new Vector3(0, 0, 25);
    }

    public void OnChangeControlVariant(bool variant)
    {
        isVariantA = variant;
    }

    public void OnChangeInverse(bool variant)
    {
        isInverseInput = variant;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
            return;

        if (isVariantA)
        {
            if (Input.GetMouseButton(0))
            {
                var inversion = isInverseInput ? -1 : 1;
                _cam.transform.rotation *= Quaternion.AngleAxis(inversion * Input.GetAxis("Mouse X") * 5, Vector3.up);
            }

            Vector3 to = _cam.transform.forward;
            Vector3 from = rig.velocity.normalized;
            to.y = 0;
            from.y = 0;
            float finish_angle = Vector3.SignedAngle(from, to, Vector3.up);
            float angle = Mathf.Lerp(0, finish_angle, 0.07f);
            rig.velocity = Quaternion.AngleAxis(angle, Vector3.up) * rig.velocity;
            _BallHolder.transform.rotation *= Quaternion.AngleAxis(angle, Vector3.up);
        }
        else
        {

            if (Input.GetMouseButton(0))
            {
                var inversion = isInverseInput ? -1 : 1;
                rig.velocity = Quaternion.AngleAxis(inversion * Input.GetAxis("Mouse X") * 5, Vector3.up) * rig.velocity;
                _BallHolder.transform.rotation *= Quaternion.AngleAxis(inversion * Input.GetAxis("Mouse X") * 5, Vector3.up);
            }

            Vector3 to = rig.velocity.normalized;
            Vector3 from = _cam.transform.forward;
            to.y = 0;
            from.y = 0;
            float finish_angle = Vector3.SignedAngle(from, to, Vector3.up);
            float angle = Mathf.Lerp(0, finish_angle, 0.04f);
            _cam.transform.rotation *= Quaternion.AngleAxis(angle, Vector3.up);
        }
        
        _Ball.transform.rotation *= Quaternion.AngleAxis(360 * 2 * Time.deltaTime * rig.velocity.magnitude / 15f, new Vector3(1, 0, 0));
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Obstacle")
        {
            Restart();
        }
    }
}
