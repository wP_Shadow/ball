﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{

    public Transform PlayerTransform;

    // Start is called before the first frame update
    void Start()
    {
        transform.position = PlayerTransform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if(PlayerTransform)
            transform.position = PlayerTransform.position;
    }
}
